# Protocol for the AnonMarker.pl script
Terry Bertozzi – March 2012

This repository contains the AnonMarker PERL script to identify low copy number sequences in Roche 454 shotgun sequences, that may be developed as phylogenetic/phylogeographic markers. If you use this PERL script (or a modified version) in your work, then please cite the publication:

**Bertozzi, T., Sanders, K.L., Sistrom, M.J. and Gardner, M.G. (2012). Anonymous nuclear loci in non-model organisms: making the most of high throughput genome surveys. _Bioinformatics_ 28, 1807-1810.**

The script was developed and used on a quad core desktop PC with 8 GB RAM running Fedora15  linux and uses publicly available software packages. The RepeatMasker and SeqClean packages can use multiple cores for processing, however, the multiple processor options have been removed from the script to make it more portable (the script can be easily modified to reinstate these options).

_Disclaimer: The processes in the script were run as discrete steps as outlined in the methods, which allowed particular steps to be skipped or modified, and are automated here for convenience. Some parts of the script may take a long time to run depending on the size of dataset being analysed and the number of filters applied. Therefore, it is recommended that you test the script on your system with a small subset of data before running the main analysis._

**Before beginning sequence analysis**
Before analysis begins, it is assumed that the user has installed and configured the following software, which can be found with a simple web search:

-	BioPerl  
-	BLAST executables form NCBI (tested with BLAST version 2.2.21)  
-	RepeatScout (tested with version 1.0.5)  
-	RepeatMasker (tested with version open-3.3.0)  
-	RepeatMasker libraries  
-	SeqClean  

The script also needs to be modified to include the paths to the various software packages once installed. The code is commented to indicate where these need to be changed and examples are provided. The minimum length of sequence to retain after masking and the e-value used for BLAST searches can also be modified.

It is recommended that you create a directory and place both the single fasta formatted file containing your 454 shotgun sequences and the script into it. Create a subdirectory called “blastdbs” and place your fasta formatted files containing sequences to be filtered out (eg mitochondrial, EST sequences, genome sequences etc) in it. 


**Running the sequence analysis**
The script requires only the name of the 454 fasta formatted file as input:

`perl AnonMarker.pl <454 sequence file>`


**Output files**
The script will create a number of directories to store intermediate files generated during processing. Once the script has finished, a fasta formatted file will be created with the same name as the original file used for the analysis (minus the extension) with the suffix “_filtered.fasta” added. The file will be located in the same directory as the original data file. The sequences in this file can be used in your favorite primer design program to develop markers for testing.

