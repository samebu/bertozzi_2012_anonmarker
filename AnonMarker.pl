#!/usr/bin/perl -w

# AnonMarker is a perl script designed to identify low copy number sequences in Roche 454
# shotgun sequences, that may be developed for phylogenetic/phylogeographic markers.
# The README.docx file that accompanies this script contains important information that is
# required to successfully run the script.
#
# created by Terry Bertozzi - March 2012

use strict;
use Bio::Seq;
use Bio::SeqIO;
use File::Path qw(make_path);
use File::Basename;
use Cwd;


#****** Paths to external programs and settings - modify to reflect your system and requirements *******

my $scoutdir = ;             #The path to the RepeatScout executables e.g "/home/bin
my $maskdir = ;  			 #The path to the RepeatMasker executables e.g "/home/bin/Repeatmasker
my $cleandir= ;       		 #The path to the SeqClean executables e.g "/home/bin/SeqClean
my $blastdir = ;             #The path to the BLASTN executable e.g "/home/bin

# Clear range to keep
my $clearRange = 450; #default is 450bp

# e value for BLAST searches
my $e_value = 1e-4; #default is 1e-4

#******* Do not modify anything below this line - unless you know what you are doing :) ******

my $usage= "AnonMarker.pl <454 sequence file>";
my $infile=shift or die $usage;
my ($name, $dir, $ext) = fileparse($infile,'\..*');

#Path to filter sequences
my $scriptdir = getcwd;
my $blastdbdir="$scriptdir/blastdbs";

#Create directory structure
print "Creating directory structure for output files...\n";
make_path("$scriptdir/anon_marker", "$scriptdir/anon_marker/repeats", "$scriptdir/anon_marker/masking", "$scriptdir/anon_marker/cleaned", "$scriptdir/anon_marker/blast_results", {
    verbose => 1,
    });

#****** Repeat finding ******
print "\nTabulating l-mers...\n";

if (system( "$scoutdir/build_lmer_table", "-sequence", "$scriptdir/$infile", "-freq", "$scriptdir/anon_marker/repeats/$name"."_freq.txt") != 0) {
    die "\nError executing build_lmer_table\n";
}
print "Finding repetitive sequences...\n";
if (system( "$scoutdir/RepeatScout", "-sequence", "$scriptdir/$infile", "-output", "$scriptdir/anon_marker/repeats/$name"."_repeats.txt" ,"-freq", "$scriptdir/anon_marker/repeats/$name"."_freq.txt") != 0) {
    die "\nError executing RepeatScout\n";
}
 
#****** Repeat masking ****** 
print "Masking repetitive sequences...\n";
if (system( "$maskdir/RepeatMasker", "-lib", "$scriptdir/anon_marker/repeats/$name"."_repeats.txt", "-dir", "$scriptdir/anon_marker/masking", "-no_is",, "$scriptdir/$infile") != 0) {
    die "\nError masking repeats\n";
}
if (system( "$maskdir/RepeatMasker", "-dir", "$scriptdir/anon_marker/masking" ,"-no_is", "-pa", "4", "$scriptdir/anon_marker/masking/$infile.masked") != 0) {
    die "\nError masking repbase libraries\n";
}

#****** Strip masked areas ****** 
chdir "$scriptdir/anon_marker/cleaned";
print "\nStripping masked sequences...\n";
if (system( "$cleandir/seqclean", "$scriptdir/anon_marker/masking/$infile.masked.masked", "-o", "$scriptdir/anon_marker/cleaned/$name"."_$clearRange".".clean", "-l", "$clearRange") != 0) {
    die "\nError stripping masked sequences\n";
}

my $searchfile= "$scriptdir/anon_marker/cleaned/$name"."_$clearRange".".clean";

#******  BLAST ******
print "\nSearching sequences... \n";

opendir(DH, $blastdbdir) or die "Can't open directory $blastdbdir";

#Check if the directory has some files in it
my @files = <$blastdbdir/*>;
my $filecount = @files;
unless ($filecount > 0){
    die "Directory $blastdbdir is empty\n";
}

my $counter =1;

while (defined(my $file = readdir(DH))) {
    next if ($file =~ /^\.\.?$/); #don't process the . and .. directories
    my ($name, $dir, $ext) = fileparse($file,'\..*');
    my $blastdb = "$name"."db";
    my $blastresult = "$scriptdir/anon_marker/blast_results/"."$name"."_blastresult.txt";

    chdir $blastdbdir;
    print "\nCurrently processing $file ($counter of $filecount)\n";  
    print "...creating BLAST database\n";
    if (system( "$blastdir/makeblastdb", "-in", "$file", "-dbtype", "nucl", "-title", "$blastdb", "-out", "$blastdbdir"."/"."$blastdb", "-hash_index",, "-parse_seqids",,) != 0) {
        die "\nError executing blast command\n";
    }

    print "...blasting\n";
    if (system( "blastn", "-task", "blastn", "-db", "$blastdbdir"."/"."$blastdb", "-query", "$searchfile", "-evalue", "$e_value", "-outfmt", "6", "-out", "$blastresult" ) != 0) {
        die "\nError executing blast command\n";
    }
    $counter++;
}
closedir(DH);

#****** Filter the BLAST hits *******
print "\nFiltering BLAST hits... \n";
my %temphash=();
my @ids;

opendir(DH, "$scriptdir/anon_marker/blast_results") or die "Can't open directory $scriptdir/anon_marker/blast_results";
while (defined(my $file = readdir(DH))) {
    next if ($file =~ /^\.\.?$/); #don't process the . and .. directories
    my ($name, $dir, $ext) = fileparse($searchfile,'\..*');
    
# parse the ids fromm the blast output
#    unless (-z $file) {
    unless (open (FH, "$scriptdir/anon_marker/blast_results/$file")) {
        die ("Can't open blast report $file\n");
    }
    while (my $line =<FH>) {
        chomp ($line);
        my @cols=split(/\s/,$line,2);
        my $id=shift(@cols);
        unless (exists($temphash{$id})){
            $temphash{$id}=0;
        }
    }
#    }
}

# create SeqIO objects to read/write fasta files     
my $seqin = Bio::SeqIO->new('-file' => "<$searchfile",
                            '-format' => 'Fasta');

my $seqout = Bio::SeqIO->new('-file' => ">$scriptdir"."/"."$name"."_filtered.fasta",
                            '-format' => 'Fasta');
    
while (my $seqobj = $seqin->next_seq) {
    if (!exists ($temphash{$seqobj->primary_id})){
        $seqout->write_seq($seqobj);
    } 
}

closedir(DH);

print "\n\nProcessing complete\n";
exit;
